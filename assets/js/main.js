
// Obtener la información del formulario
var formSearch = document.querySelector("#formSearch");

// Agregar evento al presionar botón enviar del formulario
formSearch.addEventListener('submit', (e) => {

    // Evitamos que la página se recargue
    e.preventDefault();
  
    console.log("#formulario: ", formSearch);

    // Capturar información del input
    var inputSearch = document.querySelector("#inputSearch").value;

    if(inputSearch == ''){
        alert("ingresa algo");
    } else {
        console.log("## Búsqueda: ", inputSearch);
        location.href ="./propiedades.html";
    }

});